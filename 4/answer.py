def encontrar_par(lista, suma):
    izquierda, derecha = 0, len(lista) - 1
    while izquierda < derecha:
        actual_suma = lista[izquierda] + lista[derecha]
        if actual_suma == suma:
            return lista[izquierda], lista[derecha]
        elif actual_suma < suma:
            izquierda += 1
        else:
            derecha -= 1
    return None

lista = range(1, 1000000)
suma = 500

par = encontrar_par(lista, suma)
if par:
    print(f"El par que suma {suma} es: {par[0]} y {par[1]}")
else:
    print("No se encontró ningún par que sume la cantidad dada.")